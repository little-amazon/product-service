const Router = require('express').Router;
const helloWorldController = require('./controllers/helloWorld').helloWorld;
const productController = require('./controllers/product_controller');
const productValidator = require('./validators/product_validator').createAdminUserValidators;
const productValidatorCheck = require('./middleware/product_validation_check').handleValidationResult;

//init router
const router = Router();

//test call
router.get('/', helloWorldController);

//all other calls without validation
router.get('/product', productController.allProducts);
router.delete('/product', productController.deleteAllProducts);
router.get('/product/:productID', productController.getProduct);
router.delete('/product/:productID', productController.deleteProduct);

//update product quantity
router.put('/product/:productID/:quantity', productController.incrementQuantity);
router.delete('/product/:productID/:quantity', productController.decrementQuantity);

//according to express-validator lib -> path, validator, check to return errors, req/res function if all is correct
router.post('/product', productValidator, productValidatorCheck, productController.newProduct);


module.exports = router;