const { body } = require('express-validator/check');

module.exports.createAdminUserValidators = [
  body('name', 'name is required').exists(),
  body('quantity', 'initial quantity is required').exists(),
  body('price', 'price is required or is not an int').exists().isInt()
];
