const nano = require('../db');


async function allProducts(req, res) {
    //get all products
    nano.retrieveAllProducts().then(function(body) {
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function deleteAllProducts(req, res) {
    //first get all prod
    nano.retrieveAllProducts().then(function(body) {
        
        const allRows = body.rows;

        var successCount = 0;
        for(const row of allRows) {
            //then destroy doc
            nano.destroyDoc(row.id, row.value.rev).then(function(body) {
                successCount++;

                //if last element return success
                if(successCount === allRows.length) {
                    res.status(200).send({"message" : "all deleted"});
                }
            
            }, function(err) {
                res.status(400).send(err);
                return; //to exit from for-loop
            });
        }
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function newProduct(req, res) {
    //get body
    const jsBody = req.body;

    //insert new
    nano.insertProduct(jsBody).then(function(body) {
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });
    
}

async function getProduct(req, res) {
    var pID = req.params.productID;

    nano.retrieveProduct(pID).then(function(body) {
        res.status(200).send(body);
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function deleteProduct(req, res) {
    var pID = req.params.productID;

    //first get prod to retrieve revision
    nano.retrieveProduct(pID).then(function(body) {

        //then destroy doc
        nano.destroyDoc(pID, body._rev).then(function(body) {
            res.status(200).send(body);
        
        }, function(err) {
            res.status(400).send(err);
        });
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function incrementQuantity(req, res) {
    const pID = req.params.productID;
    const quantity = req.params.quantity;

    nano.retrieveProduct(pID).then(function(body) {

        //then destroy doc
        body.quantity = Number(body.quantity) + Number(quantity);
        
        nano.insertProduct(body).then(function(body) {
            res.status(200).send(body);
        
        }, function(err) {
            res.status(400).send(err);
        });
    
    }, function(err) {
        res.status(400).send(err);
    });
}

async function decrementQuantity(req, res) {
    const pID = req.params.productID;
    const quantity = req.params.quantity;

    nano.retrieveProduct(pID).then(function(body) {

        //then destroy doc
        if(body.quantity >= quantity) {
            body.quantity = Number(body.quantity) - Number(quantity);
        
            nano.insertProduct(body).then(function(body) {
                res.status(200).send(body);
            
            }, function(err) {
                res.status(400).send(err);
            });
        
        } else {
            res.status(400).send(new Error("quantity cannot be < 0"));
        }
        
    
    }, function(err) {
        res.status(400).send(err);
    });
}



module.exports = {
    allProducts: allProducts,
    deleteAllProducts: deleteAllProducts,
    newProduct: newProduct,
    getProduct: getProduct,
    deleteProduct: deleteProduct,
    incrementQuantity: incrementQuantity,
    decrementQuantity: decrementQuantity
}