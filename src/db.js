const USERNAME = '';
const PASSWORD = '';
const IP = process.env.PRODUCT_DB_URL //'192.168.99.100';
const PORT = process.env.PRODUCT_DB_PORT //'5984';
const DB_NAME = 'product_database';

const nano = require('nano')('http://' + IP + ':' + PORT);

var db = null;

function createConnection() {
    try {
        //create nanodb if not exists
        nano.db.create(DB_NAME, function(err, body, header) {
            if (!err || err.error == 'file_exists') {
                db = nano.use(DB_NAME);
            } else if(err.code === 'ECONNREFUSED') {
                createConnection(); //recursive call
            } else {
                console.log(err);
                process.exit(1);
            }
        });
    } catch(exc) {
        createConnection();
    } 
    
}

//create db connection
createConnection();





//DELETE DOC
async function destroyDoc(dID, dRev) {
    return new Promise(function(resolve, reject) {
        db.destroy(dID, dRev, function(err, body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

//RETRIEVE ALL
async function retrieveAllProducts() {
    return new Promise(function(resolve, reject) {
        db.list({include_docs: true},function(err,body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

//RETRIEVE SINGLE
async function retrieveProduct(pID) {
    return new Promise(function(resolve, reject) {
        db.get(pID, { revs_info: false }, function(err, body) {
            if(err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

//INSERT NEW
async function insertProduct(jsBody) {
    return new Promise(function(resolve, reject) {
        db.insert(jsBody, {}, function(err, body) {
            if (err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}


module.exports = {
    destroyDoc : destroyDoc,
    retrieveAllProducts : retrieveAllProducts,
    insertProduct : insertProduct,
    retrieveProduct : retrieveProduct
};
